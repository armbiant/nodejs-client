const jose = require('jose');
const { Issuer, generators } = require('openid-client')
const CLOCK_TOLERANCE = 5 // 5 seconds
const blacklist = require('express-jwt-blacklist')

function formatUnixTimestamp(timestamp) {
    return (new Date(timestamp * 1000)).toLocaleString()
}

class OidcClientServiceClass {

    init(config) {
        console.log('Initializing OidcClientService')
        this._config = config || {}
        if (this.isDebug()) {
            console.log('OidcClientServiceClass:init(): debug mode')
        }
        // TODO: switch to memcached or redis
        const blacklistConfig = {}
    }

    isDebug() { return this._config.is_debug }
    blacklistRevoke(payload) { blacklist.revoke(payload) }
    blacklistIsRevoked(req, payload, done) { blacklist.isRevoked(req, payload, done) }

    async client() {
        if (!this._client) {
            let issuer = await this.issuer()
            const metadata = {}
            for (var prop of ['client_id', 'client_secret', 'redirect_uris', 'post_logout_redirect_uris']) {
                if (this._config[prop] === undefined) {
                    throw new Error(`Missing required config parameter for oidc Client ${prop}`)
                }
                metadata[prop] = this._config[prop]
            }

            metadata['response_types'] = ['code']

            this._client = new issuer.Client(metadata)
        }
        return this._client
    }

    async issuer() {
        if (!this._issuer) {
            if (this._config.op_metadata_url === undefined) {
                throw new Error('Missing required parameter for oidc issuer op_metadata_url')
            }
            this._issuer = await Issuer.discover(this._config.op_metadata_url)
        }
        return this._issuer
    }

    getTokenFromHeader(req) {
        if (!req.headers.authorization) {
            throw new Error('Missing authorization header')
        }
        return req.headers.authorization.replace('Bearer ', '')
    }

    async verifyJwt(jwt) {

        const issuer = await this.issuer()
        const keystore = await issuer.keystore()
        const options = {
            clockTolerance: `${CLOCK_TOLERANCE}s`
        }
        if (this._config.client_id !== undefined) {
            options.audience = this._config.client_id
        }

        const decoded = jose.JWT.verify(jwt, keystore, options)
        if (Array.isArray(this._config.required_claims)) {
            for (var prop of this._config.required_claims) {
                if (decoded[prop] === undefined) {
                    throw new Error(`JWT required claim missing: ${prop}`)
                }
            }
        }
        return decoded
    }
}

var OidcClientService = new OidcClientServiceClass()

module.exports = {
    OidcClientService
}