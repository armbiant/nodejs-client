const OidcClientService = require('./service').OidcClientService

async function getUserFromJwt(req, res, next) {
    try {

        console.log('Auth middleware')
        const token = OidcClientService.getTokenFromHeader(req)
        // This token has to be an ID token. This will not
        // work with an access token.
        // See https://github.com/AzureAD/microsoft-authentication-library-for-js/issues/815
        const payload = await OidcClientService.verifyJwt(token)
        OidcClientService.blacklistIsRevoked(req, payload, (err, isRevoked) => {
            if (err) { throw(err) }
            if (isRevoked) { throw new Error('Revoked token') }
            console.log('TOKEN IS NOT REVOKED')
            req.user = { ...payload }
            next()
        })
    } catch (err) {
        console.log(err)
        res.sendStatus(401)
        next(new Error('Unauthorized'))
    }

}

module.exports = {
    getUserFromJwt
}