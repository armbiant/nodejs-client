const express = require('express')
const router = express.Router({ mergeParams: true })
const loginLogout = require('./login-logout.controller')
const api = require('./api.controller')

const loginLogoutCtr = new loginLogout.LoginLogoutController()
const apiCtr = new api.ApiController()

router.get('/authorize', loginLogoutCtr.authorize)
router.get('/authorizationCallback', loginLogoutCtr.authorizationCallback)
router.get('/logout', loginLogoutCtr.logout)
router.get('/logoutCallback', loginLogoutCtr.logoutCallback)
router.get('/api/refresh', apiCtr.refresh)

module.exports = router