const { generators } = require('openid-client')
const OidcClientService = require('./service').OidcClientService
class LoginLogoutController {

    async authorize(req, res, next) {
        try {
            const client = await OidcClientService.client()
            const codeVerifier = generators.codeVerifier()
            req.session.oidc_code_verifier = codeVerifier
            const code_challenge = generators.codeChallenge(codeVerifier)
            if (req.query.next) {
                if (!OidcClientService.isDebug()) {
                    console.log('authorize: Should be checking domain of next, but doing nothing')
                // TODO: check the next url here to make sure it comes from the same domain
                // local dev output: Authorize request: host=localhost:5000, next=http://localhost:3000/oidclogin
                }
                req.session.oidc_next_url = req.query.next
            } else {
                req.session.oidc_next_url = ''
            }
            const authorizationUrl = client.authorizationUrl({
                scope: 'openid email profile offline_access',
                code_challenge,
                code_challenge_method: 'S256'
            })
            res.redirect(authorizationUrl)
        } catch (err) {
            next(err)
        }
    }

    async authorizationCallback(req, res, next) {
        const client = await OidcClientService.client()
        const params = client.callbackParams(req)
        const code_verifier = req.session.oidc_code_verifier
        delete req.session.oidc_code_verifier
        let tokenSet
        try {
            tokenSet = await client.callback(process.env.REDIRECT_URI, params, { code_verifier })
        } catch(err) {
            next(err)
            return
        }

        const { refresh_token, id_token } = tokenSet
        req.session.oidc_refresh_token = refresh_token
        if (req.session.oidc_next_url) {
            const next_url = decodeURI(req.session.oidc_next_url)
            const params = { id_token, refresh_token }
            const query = Object.keys(params)
                .map(key => `${key}=${params[key]}`)
                .join('&')
            const url = encodeURI(`${next_url}?${query}`)
            req.session.save(() => {
                res.header('Access-Control-Allow-Credentials', 'true')
                res.redirect(url)
            })
        } else {
            let token = ''
            if (OidcClientService.isDebug()) {
                token = `
                    <h2>ID token</h2>
                    <p>${id_token}</p>
                `
            }
            res.send(`
                <body>
                    <h1>You are logged in</h1>
                    ${token}
                </body>`)
        }
    }

    async logout(req, res) {
        const client = await OidcClientService.client()
        let id_token
        try {
            id_token = req.headers.authorization.replace("Bearer ", "")
        } catch(err) {
            if (!req.query.id_token) {
                console.log('ERROR: no id token for logout!')
            } else {
                id_token = req.query.id_token
            }
        }
        let payload
        if (id_token) {
            try {
                payload = await OidcClientService.verifyJwt(id_token)
                OidcClientService.blacklistRevoke(payload)
            } catch (err) { console.log(err) }
        }
        let state = 'http://localhost:5000/test'

        if (req.query.next) {
            console.log(`Logout request: host=${req.headers.host}, next=${req.query.next}`)
            if (!OidcClientService.isDebug()) {
                console.log('logout: Should be checking domain of next, but doing nothing')

                // TODO: validate domain
            }
            state = req.query.next
        }

        const endSessionUrl = client.endSessionUrl({
            state,
            // Without id token hint, we can log back on again without re-entering password
            id_token_hint: id_token
        })
        req.session.destroy()
        res.redirect(endSessionUrl)
    }

    async logoutCallback(req, res, next) {
        try {
            req.session.destroy()
            if (req.query.state) {
                res.redirect(req.query.state)
            } else {
                res.send('You logged out')
            }
        } catch (err) {
            next(err)
        }
    }
}

module.exports = {
    LoginLogoutController
}