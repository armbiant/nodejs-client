const OidcClientService = require('./service').OidcClientService

class ApiController {
    async refresh(req, res, next) {
        try {
            const client = await OidcClientService.client()
            if (!req.session.oidc_refresh_token) {
                throw new Error('Cannot refresh if no refresh token provided')
            }
            const oldRefresh = req.session.oidc_refresh_token
            const { refresh_token, id_token } = await client.refresh(oldRefresh)
            req.session.oidc_refresh_token = refresh_token

            res.json({ id_token })
        } catch (err) {
            console.log(err)
            res.status(400).send()
        }
    }
}



module.exports = {
    ApiController
}