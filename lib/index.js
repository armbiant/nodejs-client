module.exports = {
    OidcClientService: require('./service').OidcClientService,
    ApiController: require('./api.controller').ApiController,
    LoginLogoutController: require('./login-logout.controller').LoginLogoutController,
    getUserFromJwt: require('./middleware').getUserFromJwt,
    OidcClientRouter: require('./routes'),
}
