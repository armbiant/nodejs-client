# oidc-client

oidc-client is a server-side OpenID Relying Party (RP, Client) implementation for Node.js runtime, using [Express](https://expressjs.com/) and [express-session](https://www.npmjs.com/package/express-session). It is a thin wrapper around [openid-client](https://github.com/panva/node-openid-client) that adds the following features:
- Middleware to protect calls to API
- Persistant Issuer and Client data that avoids repeated calls to the well-known OP (Open ID Provider) metadata endpoint

## Installation

In your project's `package.json`:

```json
"dependencies": {
    "oidc-client": "git+ssh://git@gitlab.com:systra/qeto/lib/node-oidc-client.git"
}

```

On the command line in the same directory as `package.json`:

```bash
npm install --save cors
npm install --save express
npm install --save express-session
npm install
```

## Usage

### Initialization

At the start of your application, initialize OidcClientService in your server-side code:

```js
const { OidcClientService } = require('oidc-client')
OidcClientService.init({
    client_id: 'some client id', // The client ID that the OP generated when you registered your application
    client_secret: 'some client secret', // The client secrete provided by the OP
    redirect_uris: ['list of URIS...'], // One-item array containing the post-login redirect URI that you have provided to your OP.
    // For example: https://myap-back.io/auth/authorizationCallback
    post_logout_redirect_uris: ['list of URIS...'], // One-item array containing the post-logout redirect URI that you have provided to your OP.
    // For example: https://myapp-back.io/auth/logoutCallback
    op_metadata_url: 'some URL', // the URL to the Issuer's .well-known endpoint
    required_claims: ['email', 'name', 'username', '...'], // claims expected in a valid ID token that are not universal (not necessary to include 'aud', 'exp', ...)
    is_debug: false, // boolean, defaults to false
})
```



### Initialize `express-session`

The following is an example. Please see the [express-session documentation](https://www.npmjs.com/package/express-session)

```js
const session = require('express-session')
const { v4: uuidv4 } = require('uuid')
const sessionParams = {
    genid: () => uuidv4(),
    resave: false,
    secret: process.env.OP_CLIENT_SECRET,
    saveUninitialized: false,
    cookie: {}
}
app.use(session(sessionParams))
```

### Configure cross-origin requests

The following is an example only. Please see the [Express CORS middleware documentation](https://expressjs.com/en/resources/middleware/cors.html#configuration-options))

```js

const cors = require('cors')
const corsConfig = {
    origin: [
        'https://example1.com',
        'https://example2.com'
    ],
    credentials: true,
    methods: 'GET,POST,DELETE,PUT',
    allowedHeaders: 'authorization, project, content-type'
}
if ('debug' === process.env.MODE) {
    // Allow cross origin requests from anyone. Easier to debug with
    // ARC or postman, for example.
    corsConfig.origin = true
}
app.use(cors(corsConfig))
```

### Add the routes:

```js
const { OidcClientRouter } = require('oidc-client')
app.use('/auth', require('oidc-client').OidcClientRouter
```

## Routes

All routes are indicated assuming that you have prepended the routes with `/auth` as indicated above.

### Authentication (login)

```
GET /auth/authorize
```

In order to login, the browser must be redirected to the authentication route

The query must have the following parameter:
- `next`: the final redirect URI, i.e. the page that you wish to redirect to after successful login. If you have separate front and backends, `next` should point to a page in the front.

For example:

```
https://myapp-back.io/auth/authorize?next=https://myapp.io/home
```

This page is NOT the same as the one you listed in `redirect_uri` above. The `redirect_uri` is the backend handler that will then redirect to your `next`.

After successful authentication, the browser will be redirected to the indicated `next` with the `id_token` as a query parameter. For example:

```
https://myapp.io/home?id_token=ID_TOKEN_HERE
```

It is up to your frontend, or whoever will be calling your API, to store these tokens for later use.

### Logout

```
GET /auth/logout
```

In order to logout the current user, the browser must redirect to the logout route.

The query must have the following parameters:
-  `next`:  The final redirect URI, i.e. the page that you wish to redirect to after successful logout. If you have separate front and backends, `next` should point to a page in the front.
- `id_token`: A valid ID token

For example:

```
https://myapp-back.io/auth/logout?next=https://myapp.io/home&id_token=ID_TOKEN_HERE
```

### Refresh the ID Token

```
POST /auth/api/refresh
```

When your ID token has expired, you can request a new one by posting to this endpoint.

On success, the response body will contain a new ID token as follows:

```json
{
    "id_token": "NEW_ID_TOKEN_HERE"
}
```

## Verify authenticated user for API requests

The middleware protects requests to your API as follows:
- The middleware verifies the ID token that is provided in the Authorization header. (The token must be preceded by `Bearer `. For example: `Bearer ID_TOKEN_HERE`)
- If there is no ID token or if it is not valid, An error will be returned, and the request will not be executed.
- If the ID token is valid, its payload will be added as `req.user` (see [Express documentation](https://www.npmjs.com/package/express-session)).

To protect the server's routes that begin with `/api`:

```js
const { getUserFromJwt } = require('oidc-client')
app.use('/api/*', getUserFromJwt)
```

## TODO

* logout URI on Microsoft Azure AD portal?